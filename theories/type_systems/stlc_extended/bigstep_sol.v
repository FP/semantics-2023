From stdpp Require Import gmap base relations.
From iris Require Import prelude.
From semantics.ts.stlc_extended Require Import lang notation.

(** * Big-step semantics *)

Implicit Types
  (v : val)
  (e : expr).

Inductive big_step : expr → val → Prop :=
  | bs_lit (n : Z) :
      big_step (LitInt n) (LitIntV n)
  | bs_lam (x : binder) (e : expr) :
      big_step (λ: x, e)%E (λ: x, e)%V
  | bs_add e1 e2 (z1 z2 : Z) :
      big_step e1 (LitIntV z1) →
      big_step e2 (LitIntV z2) →
      big_step (Plus e1 e2) (LitIntV (z1 + z2))%Z
  | bs_app e1 e2 x e v2 v :
      big_step e1 (LamV x e) →
      big_step e2 v2 →
      big_step (subst' x (of_val v2) e) v →
      big_step (App e1 e2) v
| bs_pair e1 e2 v1 v2 :
      big_step e1 v1 →
      big_step e2 v2 →
      big_step (e1, e2) (v1, v2)
  | bs_fst e v1 v2 :
      big_step e (v1, v2) →
      big_step (Fst e) v1
  | bs_snd e v1 v2 :
      big_step e (v1, v2) →
      big_step (Snd e) v2
  | bs_injl e v :
      big_step e v →
      big_step (InjL e) (InjLV v)
  | bs_injr e v :
      big_step e v →
      big_step (InjR e) (InjRV v)
  | bs_casel e e1 e2 v v' :
      big_step e (InjLV v) →
      big_step (e1 (of_val v)) v' →
      big_step (Case e e1 e2) v'
  | bs_caser e e1 e2 v v' :
      big_step e (InjRV v) →
      big_step (e2 (of_val v)) v' →
      big_step (Case e e1 e2) v'

    .
#[export] Hint Constructors big_step : core.

Lemma big_step_of_val e v :
  e = of_val v →
  big_step e v.
Proof.
  intros ->.
  induction v; simpl; eauto.
Qed.



Lemma big_step_val v v' :
  big_step (of_val v) v' → v' = v.
Proof.
  enough (∀ e, big_step e v' → e = of_val v → v' = v) by naive_solver.
  intros e Hb.
  induction Hb in v |-*; intros Heq; subst; destruct v; inversion Heq; subst; naive_solver.
Qed.
