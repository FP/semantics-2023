From iris.proofmode Require Import tactics.
From iris.heap_lang Require Import lang notation.
From iris.base_logic.lib Require Import mono_nat.
From semantics.pl Require Import invariant_lib.
From semantics.pl.logrel Require notation syntactic logrel.
From semantics.pl Require Import ghost_state_lib.
From semantics.pl.heap_lang Require Import primitive_laws proofmode.
From iris.prelude Require Import options.
Set Default Proof Using "Type*".

(** Update rules *)
(*Check upd_return.*)
(*Check upd_bind.*)
(*Check upd_wp.*)

(** Derived rules *)
Section derived.
  Context `{heapGS Σ}.
  Implicit Types (P Q : iProp Σ).

  Lemma upd_wand P Q :
    (|==> P) -∗
    (P -∗ Q) -∗
    |==> Q.
  Proof.
    iIntros "HP HPQ".
    iApply (upd_bind with "HP").
    iIntros "HP". iApply upd_return.
    by iApply "HPQ".
  Qed.

  Lemma upd_mono P Q :
    (P ⊢ Q) →
    (|==> P) ⊢ |==> Q.
  Proof.
    iIntros (HPQ) "HP". iApply (upd_bind with "HP").
    iIntros "HP". iApply upd_return. by iApply HPQ.
  Qed.

  Lemma upd_trans P :
    (|==> |==> P) ⊢ |==> P.
  Proof.
    iIntros "HP".
    iApply (upd_bind with "HP").
    iIntros "$".
  Qed.

  Lemma upd_frame P Q :
    P -∗ (|==> Q) -∗ |==> (P ∗ Q).
  Proof.
    iIntros "HP HQ".
    iApply (upd_bind with "HQ [HP]").
    iFrame "HP". iIntros "HQ". iApply upd_return.
    iFrame.
  Qed.
End derived.

(** ** The mono nat ghost theory *)
(*Check mono.*)
(*Check lb.*)

(*Check mono_nat_make_bound.*)
(*Check mono_nat_use_bound.*)
(*Check mono_nat_increase_val.*)
(*Check mono_nat_new.*)

(* The lb resource is persistent *)
(*Check mono_nat_lb_persistent.*)

(* Both resources are also timeless *)
(*Check mono_nat_lb_timeless.*)
(*Check mono_nat_mono_timeless.*)

Section mono_derived.
(** In addition to the known [heapGS] assumption, we now also need to assume that the ghost state for the theory of mono_nat has been registered with Iris, expressed through the [mono_natG] assumption.
 *)
  Context `{heapGS Σ} `{mono_natG Σ}.
  Implicit Types (P Q : iProp Σ).

  Lemma mono_nat_increase γ n m :
    n ≤ m →
    mono γ n -∗ |==> mono γ m.
  Proof.
    intros Hle.
    assert (m = (m - n) + n) as -> by lia.
    generalize (m - n) as k. intros k.
    iInduction (k) as [ | k ] "IH".
    - iApply upd_return.
    - iIntros "Hn". iMod ("IH" with "Hn") as "Hm".
      simpl; by iApply mono_nat_increase_val.
  Qed.

  Lemma mono_nat_increase_wp γ n m e Φ :
    n ≤ m →
    (mono γ m -∗ WP e {{ Φ }}) -∗
    (mono γ n -∗ WP e {{ Φ }}).
  Proof.
    iIntros (Hle) "He Hauth".
    iApply upd_wp. iApply (upd_wand with "[Hauth]"); last iApply "He".
    by iApply mono_nat_increase.
  Qed.

  Lemma mono_nat_new_wp e Φ n :
    (∀ γ, mono γ n -∗ WP e {{ Φ }}) -∗ WP e {{ Φ }}.
  Proof.
    iIntros "Hwand".
    iApply upd_wp. iApply upd_wand; first iApply mono_nat_new.
    iIntros "(%γ & Hauth)". by iApply "Hwand".
  Qed.
End mono_derived.

(** ** Updates in the IPM *)
Section ipm.
  Context `{heapGS Σ} `{mono_natG Σ}.
  Implicit Types (P Q : iProp Σ).

  Lemma update_ipm_1 γ n :
    mono γ 0 -∗
    |==> mono γ n.
  Proof.
    iIntros "Hauth".
    (* [iMod] will eliminate an update modality in front of the hypothesis (using [upd_bind]) *)
    iMod (mono_nat_increase _ _ n with "Hauth") as "Hauth"; first lia.
    (* [iFrame] can also frame hypotheses below an update, using monotonicity *)
    iFrame "Hauth". done.
    Restart.
    iIntros "Hauth".
    iMod (mono_nat_increase with "Hauth") as "$"; [lia | done].
  Abort.

  Lemma update_ipm_2 γ n e Φ  :
    mono γ 0 -∗
    (mono γ n -∗ WP e {{ Φ }}) -∗
    WP e {{ Φ }}.
  Proof.
    iIntros "Hauth He".
    (* [iMod] will automatically apply [upd_wp] to eliminate updates at a WP *)
    iMod (mono_nat_increase with "Hauth") as "Hauth"; first lia.
    by iApply "He".
  Abort.

  Lemma update_ipm_3 n γ :
    (|==> mono γ n) -∗ |==> mono γ (S n).
  Proof.
    (* the [>] intro pattern will eliminate an update when introducing an assumption *)
    iIntros ">Hauth". by iApply mono_nat_increase_val.
  Abort.
End ipm.


(** The Symbol ADT *)
Section logrel_symbol.
  Import logrel.notation syntactic logrel.
  Context `{heapGS Σ} `{mono_natG Σ}.

  Definition assert e := (if: e then #() else #0 #0)%E.

  Definition symbolT : type := ∃: ((Unit → #0) × (#0 → Unit)).
  Definition mk_symbol : expr :=
    let: "c" := ref #0 in
    pack (λ: <>, let: "x" := !"c" in "c" <- "x" + #1;; "x", λ: "y", assert ("y" ≤ !"c")).

  Definition symbolN := nroot .@ "symbol".

  Definition mono_nat_inv γ l : iProp Σ :=
    ∃ n : nat, l ↦ #n ∗ mono γ n.
  Definition mono_natT γ : semtypeO :=
    mk_semtype (λ v, ∃ (n : nat), ⌜v = #n⌝ ∗ lb γ n)%I.

  Lemma mk_symbol_semtyped :
    TY 0; ∅ ⊨ mk_symbol : symbolT.
  Proof using Type*.
    iIntros (δ γ) "#Hctx".
    iPoseProof (context_interp_dom_eq with "Hctx") as "%Hdom".
    rewrite dom_empty_L in Hdom. symmetry in Hdom. apply dom_empty_iff_L in Hdom.
    subst γ. rewrite subst_map_empty. unfold mk_symbol. simpl.
    wp_alloc l as "Hl". wp_pures.
    (* We allocate the ghost state + invariant *)
    iMod (mono_nat_new 0) as "(%γ & Hauth)".
    iApply (inv_alloc symbolN (mono_nat_inv γ l) with "[Hauth Hl]").
    { unfold mono_nat_inv. eauto with iFrame. }
    iIntros "#HInv".
    iApply wp_value.
    iExists _. iSplitR; first done.
    iExists (mono_natT γ), _, _. iSplitR; first done. iSplit.
    - (* mkSym *)
      iIntros (w) "!> ->".
      wp_pures. iApply (inv_open with "HInv"); first set_solver.
      iIntros "(%n & >Hl & Hauth)".
      wp_load. wp_store.
      iPoseProof (mono_nat_make_bound with "Hauth") as "#Hbound".
      iMod (mono_nat_increase _ _ (n + 1) with "Hauth") as "Hauth"; first lia.
      iApply wp_value. simpl. iSplitL.
      + unfold mono_nat_inv. iNext. iExists (n + 1). iFrame.
        replace (Z.of_nat (n +1)%nat) with (n + 1)%Z by lia; done.
      + eauto with iFrame.
    - (* get *)
      simpl. iIntros (w) "!> (%n & -> & Hlb)". wp_pures.
      iApply (inv_open with "HInv"); first set_solver.
      iIntros "(%m & >Hl & Hauth)".
      wp_load.
      iPoseProof (mono_nat_use_bound with "Hauth Hlb") as "%Hleq".
      wp_pures.  rewrite bool_decide_eq_true_2; last lia.
      wp_pures. iApply wp_value. unfold mono_nat_inv. eauto with iFrame.
  Qed.
End logrel_symbol.


(** Exercise: Oneshot *)
Section logrel_oneshot.
  Import logrel.notation syntactic logrel.
  Context `{heapGS Σ} `{oneshotG Σ nat}.


  (*Check os_pending_alloc.*)
  (*Check os_pending_shoot.*)
  (*Check os_shot_persistent.*)
  (*Check os_pending_shot_False.*)
  (*Check os_pending_pending_False.*)
  (*Check os_shot_agree.*)
  (*Check os_shot_timeless.*)
  (*Check os_pending_timeless.*)

  Definition code : expr :=
    let: "x" := ref #42 in
    λ: "f", "x" <- #1337;;
            "f" #();;
            assert (!"x" = #1337).

  Definition codeN := nroot .@ "oneshot".

  Lemma code_safe :
    TY 0; ∅ ⊨ code : ((TUnit → TUnit) → TUnit).
  Proof.
    iIntros (δ γ) "#Hctx".
    iPoseProof (context_interp_dom_eq with "Hctx") as "%Hdom".
    rewrite dom_empty_L in Hdom. symmetry in Hdom. apply dom_empty_iff_L in Hdom.
    subst γ. rewrite subst_map_empty. unfold code. simpl.

    wp_alloc l as "Hl". wp_pures.
    iMod os_pending_alloc as (γ) "Hpend".
    set (I := ((l ↦ #42 ∗ os_pending γ) ∨ (l ↦ #1337 ∗ os_shot γ 4))%I).
    iApply (inv_alloc codeN I with "[Hl Hpend]").
    { iLeft. iFrame. }

    iIntros "#Hinv". iApply wp_value. iIntros (w) "!> Happ".
    wp_pures.
    wp_bind (_ <- _)%E.
    iApply (inv_open with "Hinv"); first set_solver.
    iIntros "[(>Hl & >Hpend) | (>Hl & Hshot)]".
    - wp_store. iMod (os_pending_shoot _ 4 with "Hpend") as "#Hshot".
      iApply wp_value. iSplitL "Hl".
      { iRight. eauto with iFrame. }
      wp_pures. wp_bind (w _).
      iApply (wp_wand with "(Happ [//])").
      iIntros (v) "->". wp_pures.

      iApply (inv_open with "Hinv"); first set_solver.
      iIntros "[(>Hl & >Hpend) | (>Hl & _)]".
      { iExFalso. iApply (os_pending_shot_False with "Hpend Hshot"). }
      wp_load. wp_pures. iApply wp_value. iSplitL; last done. iRight; eauto with iFrame.
    - (* already shot, but this is also fine *)
      wp_store. iDestruct "Hshot" as "#Hshot".
      iApply wp_value. iSplitL "Hl".
      { iRight. eauto with iFrame. }
      wp_pures. wp_bind (w _).
      iApply (wp_wand with "(Happ [//])").
      iIntros (v) "->". wp_pures.

      iApply (inv_open with "Hinv"); first set_solver.
      iIntros "[(>Hl & >Hpend) | (>Hl & _)]".
      { iExFalso. iApply (os_pending_shot_False with "Hpend Hshot"). }
      wp_load. wp_pures. iApply wp_value. iSplitL; last done. iRight; eauto with iFrame.
  Qed.

End logrel_oneshot.

(** Exercise: Agreement *)
Section logrel_ag.
  Import logrel.notation syntactic logrel.
  Context `{heapGS Σ} `{halvesG Σ Z}.

  (*Check ghalves_alloc.*)
  (*Check ghalves_agree.*)
  (*Check ghalves_update.*)
  (*Check ghalf_timeless.*)

  Definition rec_code : expr :=
    λ: "f",
    let: "l" := ref #0 in
    (rec: "loop" <> :=
        let: "x" := !"l" in
        if: "f" (λ: <>, !"l")
        then
          assert (!"l" = "x");;
          "l" <- "x" + #1;;
          "loop" #()
        else !"l")
      #().

  Lemma rec_code_safe :
    TY 0 ; ∅ ⊨ rec_code : (((TUnit → TInt) → TBool) → TInt).
  Proof.
    iIntros (δ γ) "#Hctx".
    iPoseProof (context_interp_dom_eq with "Hctx") as "%Hdom".
    rewrite dom_empty_L in Hdom. symmetry in Hdom. apply dom_empty_iff_L in Hdom.
    subst γ. rewrite subst_map_empty. unfold rec_code. simpl.

    wp_pures. iApply wp_value. iIntros (w) "!> #Happ".
    wp_pures. wp_alloc l as "Hl".
    wp_pure _. wp_pure _.

    iMod (ghalves_alloc 0%Z) as (γ) "[Half1 Half2]".
    set (I := (∃ n : Z, l ↦ #n ∗ ghalf γ n)%I).
    iApply (inv_alloc codeN I with "[Hl Half2]").
    { iExists 0. iFrame. }
    iIntros "#Hinv".

    wp_pure _.
    set (r := RecV _ _ _).
    generalize 0%Z as n. iIntros (n).
    iLöb as "IH" forall (n).
    unfold r at 2. wp_pures.
    wp_bind (! _)%E.
    iApply (inv_open with "Hinv"); first set_solver.
    iIntros "(%m & >Hl & >Half2)".
    wp_load. iApply wp_value. iDestruct (ghalves_agree with "Half1 Half2") as %<-.
    iSplitL "Hl Half2".
    { unfold I. eauto with iFrame. }
    wp_pures. wp_bind (w _).
    iApply (wp_wand).
    { iApply "Happ". iIntros (w') "!> ->".
      wp_pures. iApply (inv_open with "Hinv"); first set_solver.
      iIntros "(%m & >Hl & Half2)". wp_load. iApply wp_value.
      iSplitL; last eauto. iExists m; eauto with iFrame.
    }
    iIntros (v) "(%b & ->)". destruct b; wp_pures.
    - wp_bind (!_)%E.
      iApply (inv_open with "Hinv"); first set_solver.
      iIntros "(%m & >Hl & Half2)".
      wp_load. iDestruct (ghalves_agree with "Half1 Half2") as %->.
      iApply wp_value. iSplitL "Half2 Hl".
      { unfold I; eauto with iFrame. }
      wp_pures. rewrite bool_decide_eq_true_2; last done.
      wp_pures. wp_bind (_ <- _)%E.

      iApply (inv_open with "Hinv"); first set_solver.
      iIntros "(%n' & >Hl & Half2)".
      wp_store. iMod (ghalves_update _ _ _ (m + 1)%Z with "Half1 Half2") as "[Half1 Half2]".
      iApply wp_value.
      iSplitL "Hl Half2". { unfold I. eauto with iFrame. }

      wp_pure _. wp_pure _. fold r. by iApply "IH".
    - iApply (inv_open with "Hinv"); first set_solver.
      iIntros "(%m & >Hl & Half2)".
      wp_load. iApply wp_value.
      unfold I; eauto with iFrame.
  Qed.
End logrel_ag.

(** Exercise: Red/blue *)
Section logrel_redblue.
  Import logrel.notation syntactic logrel.
  Inductive colour := red | blue.

  Context `{heapGS Σ} `{agmapG Σ nat colour}.

  (*Check agmap_auth_alloc_empty.*)
  (*Check agmap_auth_insert.*)
  (*Check agmap_auth_lookup.*)
  (*Check agmap_elem_agree.*)
  (*Check agmap_elem_persistent.*)
  (*Check agmap_elem_timeless.*)
  (*Check agmap_auth_timeless.*)

  Definition redT γ : semtypeO :=
    mk_semtype (λ v, ∃ n : nat, ⌜v = #n⌝ ∗ agmap_elem γ n red)%I.
  Definition blueT γ : semtypeO :=
    mk_semtype (λ v, ∃ n : nat, ⌜v = #n⌝ ∗ agmap_elem γ n blue)%I.

  Definition mkColourGen : expr :=
    let: "c" := ref #0 in
    (λ: <>, let: "red" := !"c" in "c" <- #1 + "red";; "red",
     λ: <>, let: "blue" := !"c" in "c" <- #1 + "blue";; "blue",
     λ: "red" "blue", assert ("red" ≠ "blue")).

  Lemma mkColourGen_safe :
    TY 0; ∅ ⊨ mkColourGen : (∃: ∃: ((TUnit → #0) × (TUnit → #1)) × (#0 → #1 → TUnit)).
  Proof.
    iIntros (δ γ) "#Hctx".
    iPoseProof (context_interp_dom_eq with "Hctx") as "%Hdom".
    rewrite dom_empty_L in Hdom. symmetry in Hdom. apply dom_empty_iff_L in Hdom.
    subst γ. rewrite subst_map_empty. unfold mkColourGen. simpl.

    wp_alloc l as "Hl". wp_pures.
    iMod agmap_auth_alloc_empty as "(%γ & Hag)".

    set (I := (∃ (n : nat) (M : gmap nat colour),
      l ↦ #n ∗ agmap_auth γ M ∗ ⌜∀ k : nat, is_Some (M !! k) → k < n⌝)%I).
    iApply (inv_alloc codeN I with "[Hl Hag]").
    { iExists 0, ∅. iFrame. iPureIntro. intros ? []%lookup_empty_is_Some. }
    iIntros "#Hinv".

    iApply wp_value. iExists _. iSplitR; first done.
    iExists (blueT γ), _. iSplitR; first done.
    iExists (redT γ), _, _. iSplitR; first done.
    simpl. iSplit; first (iExists _, _; iSplitR; first done; iSplit).
    - (* mkRed *)
      iIntros (?) "!> ->". wp_pures.
      iApply (inv_open with "Hinv"); first set_solver.
      iIntros "(%n & %M & >Hl & >Hauth & >%Hi)".
      wp_load. wp_pures. wp_store.
      iMod (agmap_auth_insert n red with "Hauth") as "(Hauth & Hred)".
      { destruct (M !! n) as [c | ] eqn:Heq; last done.
        apply mk_is_Some in Heq. apply Hi in Heq. lia.
      }
      iApply wp_value. simpl.
      iSplitR "Hred"; last eauto with iFrame.
      iExists (1 + n), _. iFrame "Hauth".
      replace (1 + Z.of_nat n)%Z with (Z.of_nat (1 + n)) by lia. iFrame "Hl".
      iPureIntro. intros k.
      destruct (decide (k = n)) as [-> | ].
      + rewrite lookup_insert. lia.
      + rewrite lookup_insert_ne; last done. intros ?%Hi. lia.
    - (* mkBlue *)
      iIntros (?) "!> ->". wp_pures.
      iApply (inv_open with "Hinv"); first set_solver.
      iIntros "(%n & %M & >Hl & >Hauth & >%Hi)".
      wp_load. wp_pures. wp_store.
      iMod (agmap_auth_insert n blue with "Hauth") as "(Hauth & Hblue)".
      { destruct (M !! n) as [c | ] eqn:Heq; last done.
        apply mk_is_Some in Heq. apply Hi in Heq. lia.
      }
      iApply wp_value. simpl.
      iSplitR "Hblue"; last eauto with iFrame.
      iExists (1 + n), _. iFrame "Hauth".
      replace (1 + Z.of_nat n)%Z with (Z.of_nat (1 + n)) by lia. iFrame "Hl".
      iPureIntro. intros k.
      destruct (decide (k = n)) as [-> | ].
      + rewrite lookup_insert. lia.
      + rewrite lookup_insert_ne; last done. intros ?%Hi. lia.
    - (* check *)
      iIntros (w) "!> (%n & -> & #Hred)". wp_pures. iApply wp_value.
      iIntros (w) "!> (%m & -> & #Hblue)". wp_pures.
      destruct (decide (n = m)) as [<- | Hneq].
      + (* we can rule this out *)
        iPoseProof (agmap_elem_agree with "Hred Hblue") as "%Heq". discriminate Heq.
      + rewrite bool_decide_eq_false_2; last naive_solver.
        wp_pures. iApply wp_value; done.
  Qed.
End logrel_redblue.
